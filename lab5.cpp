/*
  Mason Fox
  mgfox
  Lab 5
  Lab Section 3
  Nushrat Humaira
*/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"


using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed belowsort(&deck[0], &deck[52], suit_order);
  /*This is to seed the random generator */
  //assign random seed for shuffling
  srand(unsigned (time(0)));
  int i = 0;

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card deck[52];
   //for loop to iterate through each 52 card deck
   for (i = 0; i < 52; i++){
     //for each set of 13 cards, change the suit
      if (i < 13){
        //give each spade chard, assign it a value 2-14
        deck[i].suit = SPADES;
        deck[i].value = (i%13)+2;
      }else if (i < 26){
          //give each heart chard, assign it a value 2-14
        deck[i].suit = HEARTS;
        deck[i].value = (i%13)+2;
      }else if(i < 39){
          //give each diamond chard, assign it a value 2-14
        deck[i].suit = DIAMONDS;
        deck[i].value = (i%13)+2;
      }else{
          //give each club chard, assign it a value 2-14
        deck[i].suit = CLUBS;
        deck[i].value = (i%13)+2;
      }
   }

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
   //shuffle deck array using random seed
   random_shuffle(&deck[0], &deck[52], myrandom);


   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //create hand array composed of first 5 elements of shuffled deck array
    Card hand[5] = {deck[1], deck[2], deck[3], deck[4], deck[5]};


    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //call suit order function to sort hand array
     sort(&hand[0], &hand[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */
     //loop to print each element in hand array into terminal with correct formatting
     for (i = 0; i < 5;i++){
       cout << setw(10) << right << get_card_name(hand[i]) << " of" << get_suit_code(hand[i]) << endl;
     }
  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  if (lhs.suit < rhs.suit){
    //return true if right suit is greater than left
    return true;
  }else if (lhs.suit == rhs.suit){
    //if they are equal check which value is hgiher
    if (lhs.value < rhs.value){
      return true;
    }else{
      return false;
    }
  }else {
    //otherwise return false
    return false;
  }

}

string get_suit_code(Card& c) {
  switch (c.suit) {//return code for symbols depending on the suit of the card
    case SPADES:    return " \u2660";
    case HEARTS:    return " \u2661";
    case DIAMONDS:  return " \u2662";
    case CLUBS:     return " \u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
    if (c.value < 11){
      //if the value is under 10, return string of value
      return to_string(c.value);
    }else{
      //if value is over 10, return respective card jack-ace
      if (c.value == 11){
        return "Jack";
      }else if (c.value == 12){
        return "Queen";
      }else if (c.value == 13){
        return "King";
      }else{
        return "Ace";
      }
    }
}
